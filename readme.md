Carbon Android take home challenge

Instructions
Build an Android App displaying a list of items and their detail when clicking on it. You
can use any API of your choice or themoviedb API - https://www.themoviedb.org/ . Note that you can use any library/dependency of your choice to achieve your goal.

The solution was developed using a post and message api provided by typicode.com

What to check out for

● Unit tests
● MVVM Architecture
● Clear and clean code
● Offline cache
● multi-module
● Kotlin
● Dependency Injection
● Good UI
● CI