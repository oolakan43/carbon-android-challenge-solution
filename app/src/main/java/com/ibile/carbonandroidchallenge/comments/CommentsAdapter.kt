package com.ibile.carbonandroidchallenge.data

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibile.carbonandroidchallenge.databinding.AdapterCommentsBinding
import com.ibile.mydata.data.comment.Comment

class CommentsAdapter(private var listOfComments: List<Comment>) : RecyclerView.Adapter<CommentViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val binding = AdapterCommentsBinding.inflate(LayoutInflater.from(parent.context) ,parent, false)

        return CommentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listOfComments.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindView(listOfComments[position])
    }

    fun updateComments(comments: List<Comment>) {
        listOfComments = comments
        notifyDataSetChanged()
    }
}

class CommentViewHolder(val binding: AdapterCommentsBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindView(comment: Comment) {
        itemView.apply {
            binding.name.text = comment.name
            binding.commentBody.text = comment.body
            binding.email.text = comment.email
        }
    }
}
