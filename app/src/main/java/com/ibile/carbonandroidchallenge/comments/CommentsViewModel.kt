package com.ibile.carbonandroidchallenge.comments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ibile.appdata.data.CouroutineViewModel
import com.ibile.carbonandroidchallenge.data.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class CommentsViewModel(private val repository: Repository, uiContext: CoroutineContext = Dispatchers.Main) :
    CouroutineViewModel(uiContext) {

    private val privateState = MutableLiveData<CommentsState>()

    val commentsLiveData: LiveData<CommentsState> = privateState

    fun getComments(id: Int) = launch {
        privateState.value = CommentsState.Loading
        Timber.d(privateState.value.toString())
        privateState.value = repository.getCommentsForPosts(id)
        Timber.d(privateState.value.toString())
    }

}