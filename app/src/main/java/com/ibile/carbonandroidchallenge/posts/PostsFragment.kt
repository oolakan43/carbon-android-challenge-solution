package com.ibile.carbonandroidchallenge.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.ibile.appdata.data.MainThreadScope
import com.ibile.carbonandroidchallenge.R
import com.ibile.carbonandroidchallenge.databinding.FragmentPostsBinding
import com.ibile.mydata.data.post.Post
import com.ibile.mydata.data.post.PostList
import com.ibile.mydata.data.sharedpreference.SharedPreference
import com.ibile.mydata.utils.Constant
import org.koin.androidx.viewmodel.ext.android.getViewModel


class PostsFragment : Fragment(), OnPostClickListener {

    private val uiScope = MainThreadScope()
    private var postsViewModel: PostsViewModel? = null

    private var _binding: FragmentPostsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(uiScope)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPostsBinding.inflate(inflater, container, false)
        val view = binding.root

        return view;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(false)
            setHomeButtonEnabled(false)
            title = getString(R.string.posts)
            show()
        }

        val postAdapter = PostAdapter(emptyList(), this)
        binding.postsRecyclerView?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            adapter = postAdapter
        }

        postsViewModel = getViewModel<PostsViewModel>()
        postsViewModel?.postLiveData?.observe(viewLifecycleOwner, Observer { postState ->
            if (postState == null) {
                return@Observer
            }

            when (postState) {
                is PostsState.Loading -> {
                    setUpdateLayoutVisibilty(View.VISIBLE)
                }

                is PostsState.Error -> {
                    setUpdateLayoutVisibilty(View.GONE)
                    context?.let {
                        val message = postState.message ?: getString(R.string.error)
                        val posts = SharedPreference.getPostLists(requireContext(), Constant.POST);
                        posts?.let {
                            setUpdateLayoutVisibilty(View.GONE)
                            postAdapter.updatePosts(posts.postList)
                        } ?: run {
                            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
                        }
                    }
                }

                is PostsState.PostsLoaded -> {
                    setUpdateLayoutVisibilty(View.GONE)
                    val postList = PostList(postState.posts)

                    SharedPreference.setPreferenceObject(requireContext(), postList, Constant.POST)
                    postAdapter.updatePosts(postState.posts)
                }
            }
        })

        postsViewModel?.refreshPosts()
    }

    override fun postClicked(post: Post) {
        findNavController().navigate(PostsFragmentDirections.actionPostsFragmentToCommentsFragment(post.id))
    }

    fun setUpdateLayoutVisibilty(value: Int) {
        binding.updateLayout.apply {
            visibility = value
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }

}
